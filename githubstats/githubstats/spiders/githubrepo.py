import csv
import scrapy


class GithubrepoSpider(scrapy.Spider):
    name = 'githubrepo'
    allowed_domains = ['github.com']
    start_urls = []
    repo_list = []
    repo_data = {
        'name': '',
        'http_clone_url': '',
        'date_latest_commit': '',
        'author_latest_commit': '',
    }

    def __init__(self, input_repo_file=None, *args, **kwargs):
        self.log("input repo file: {}".format(input_repo_file,))
        for repo in open(input_repo_file, 'r').readlines():
            self.start_urls.append('https://github.com/{}'.format(repo))

    def parse(self, response):
        name = response.xpath('/html/body/div[4]/div/main/div[1]/div[1]/div/h1/strong/a/text()').get()
        if name:
            self.repo_data['name'] = name

        http_clone_url = response.xpath('/html/body/div[4]/div/main/div[2]/div/div[2]/div[1]/div[1]/span/get-repo-controller/details/div/div/div[1]/div[1]/div/input/@value').get()
        if http_clone_url:
            self.repo_data['http_clone_url'] = http_clone_url

        date_latest_commit = response.xpath('/html/body/div[4]/div/main/div[2]/div/div[2]/ol[1]/li[1]/div[1]/div[1]/div[2]/relative-time/@datetime').get()
        if date_latest_commit:
            self.repo_data['date_latest_commit'] = date_latest_commit

        author_latest_commit = response.xpath('/html/body/div[4]/div/main/div[2]/div/div[2]/ol[1]/li[1]/div[1]/div[1]/div[1]/div/a/@href').get()
        if author_latest_commit:
            self.repo_data['author_latest_commit'] = author_latest_commit[1:]

        commit_url = response.xpath('/html/body/div[4]/div/main/div[2]/div/div[2]/div[1]/div[2]/div/div/div/ul/li[1]/a/@href').get()
        yield scrapy.Request(
            'https://github.com{}'.format(commit_url,),
            callback=self.parse
        )
        if all(self.repo_data.values()):
            yield self.repo_data
