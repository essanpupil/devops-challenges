# Github Stats

## Task
- Write an application/script that accepts a list of public Github repositories  and prints out the name, clone URL, date of latest commit and name of latest author for each one.

## Input
- Read plain text list of repositories from stdin.
- One repo per input line, format: $orgname/$repo, e.g. kubernetes/charts.
- Other parameters/env vars as needed, should be documented.

## Output
- One line per input repo in CSV or TSV format plus one header line to stdout.

## Notes
- Solutions can be done in Python, Golang, NodeJS or Bash.
- Solution should include dependency management for the language chosen.
- Please provide a Dockerfile.

## Solution
1. Clone this repo: `git clone https://gitlab.com/essanpupil/devops-challenges.git`
1. Build the docker image: `docker build -t githubstats ./githubstats`
1. Create file with the following name, `input_repo.txt`
1. Add this text to the file:
   ```
   essanpupil/autowebsearch
   essanpupil/pygoogling
   kubernetes/kubernetes
   ```
1. Run the docker image using this command: `docker run -v $PWD:/tmp githubstats "/tmp/input_repo.txt"`
1. Check the output in your current directory in file `repo_data.csv`
