# Nginxlog
## Task
- Deploy a Dockerized application serving a static website via (e.g. via Nginx) that displays a custom welcome page (but not the default page for the web server used).
- Use fluentd to ship Nginx request logs to an output of your choice (e.g. S3, ElasticSearch).
- Provide a standalone solution including both webapp and fluentd using docker-compose and/or a kubernetes deployment (plain manifest or helm chart).

## Notes
- Avoid running multiple services in single container.
- You can use any 3rd party Docker image (you might have to explain your choice)
- Bonus: use an IAC tool of your choice to create cloud resources you may need (e.g. S3 buckets).

## Solution
1. Clone this repo: `git clone https://gitlab.com/essanpupil/devops-challenges.git`
2. Go to project directory: `cd nginxlog`
3. Start docler cluster: `docker-compose up -d --build`
4. Open browser and go to this address to check web application: `http://127.0.0.1:1337`
5. Open another browser window and go to this address to check logging: `http://localhost:5601`
